@isTest
public  class SurveysTest {
    @isTest
    static void test_positive_01(){

        Set<String> emails = new Set<String>();
        List<Contact> contacts = new List<Contact>();
        List<Survey__c> surveys = new List<Survey__c>();
        String email = 'vasja@inc.com';
        for (Integer i = 0; i<10; i++){
            email += i;
            emails.add(email);
            contacts.add(new Contact(LastName = 'Vasylciv', Email = email));
            surveys.add(new Survey__c(Email__c = email, Comment__c = 'comment ' + i));

        }

        Test.startTest();
        insert contacts;
        insert surveys;

        Map<Id,Contact> contactMapToCheck = new Map<Id,Contact>([select id, email from Contact where email =: emails]);
        List<Survey__c> surveyListToCheck = [select Contact__c from Survey__c where Email__c =: emails];

        Test.stopTest();

        for (Survey__c s : surveyListToCheck){
            if (contactMapToCheck.containsKey(s.Contact__c)){
                System.assertEquals(s.Contact__c, contactMapToCheck.get(s.Contact__c).id);
            }
        }
    }

    @isTest
    static void test_negative_02(){
        Contact c = new Contact(LastName = 'Vasylciv', Email = 'vasja@inc.com');
        Test.startTest();
        insert c;
        Survey__c s = new Survey__c(Email__c = 'vasya@inc.com');
        insert s;

        List<Survey__c> sList = [select id, Email__c, Contact__c from Survey__c where email__c like 'vas%'];
        Contact contact = [select id, Email from Contact where LastName = 'Vasylciv' limit 1];
        Test.stopTest();

        System.assertEquals(null, sList[0].Contact__c);
        System.assertNotEquals(contact.Email, sList[0].Email__c);
    }
}