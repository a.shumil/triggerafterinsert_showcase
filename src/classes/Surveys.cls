public class Surveys {
    public static void entry (TriggerParams triggerParams) {

        Map <Id, Survey__c> newMap = (Map <Id, Survey__c>)triggerParams.newMap;
        Map <Id, Survey__c> oldMap = (Map <Id, Survey__c>)triggerParams.oldMap;
        List <Survey__c> triggerNew = (List <Survey__c>)triggerParams.triggerNew;
        List <Survey__c> triggerOld = (List <Survey__c>)triggerParams.triggerOld;

        if (triggerParams.isAfter) {
            if (triggerParams.isInsert){
                getContactLookup(triggerNew);
            }
        }
    }

    public static void getContactLookup(List<Survey__c> sList) {
        List<Contact> contacts = [select id, email from Contact where email != null];
        Map<String, Contact> contactsByEmail = new Map<String, Contact>();

        if (!contacts.isEmpty()){
            for (Contact c : contacts){
                contactsByEmail.put(c.Email, c);
            }

            List<Survey__c> listToUpdate = [select id, Contact__c, email__c from Survey__c where id =: sList];

            for(Survey__c s : listToUpdate){
                if (contactsByEmail.containsKey(s.Email__c)){
                    /*Contact contact = contactsByEmail.get(s.Email__c);
                    s.Contact__c = contact.id;*/
                    s.Contact__c = contactsByEmail.get(s.Email__c).id;

                }
            }
            update listToUpdate;
        }
    }


}